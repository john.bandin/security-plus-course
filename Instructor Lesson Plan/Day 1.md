# Instructor Lesson Plan - Day 1

### Summary 

- These lesson plans are a guide for how the class should be instructed. Any instructions that are in **Bold** are requirements, any instructions not in Bold are suggestions depending on the flow of the course. The material is always the same, but not every class is the same. Use your best judgement for how you want to instruct. 
- Day 1 should end at Domain 1.6. If you run out of time before Domain 1.6 that is fine.
- Majority of the bullet points on the slides should be broken down on a whiteboard or through demonstration. Watch my YouTube videos to see how i break down the topics

### Software

- We give our instructors the option to use a Huion Tablet and the 

### Class breaks and hours

- Class starts at 0900 and ends at 1600. However, we do train to standard and not time. So if class ends early that is okay.
- Trepa Technologies does not teach for more than 25 minutes at a time. Our teaching flow is 20 - 25 minutes of instruction and 10 minute breaks. We do not want our students going into "TV" Mode.


### 0900 - 0925

- During the first twenty - twenty-five minutes of class this will be admin period. Introduce yourself, go over your background, and the goals of this class.
- We have two major goals for training. One is to make sure students leave our class actually learning something and getting hands-on experience. Two is to make sure that students are performing there practice exams every night and acheiving an 80% or higher on majority of the exams before testing.
- Create a signal group chat and have the students fill out a roster. Get their full name, personal email, and phone number. 
- Enroll students if they have not been already enrolled in the course. See this video for instructions on how to do that.
- Work through any registration issues and then send the students on break NLT 0925

### 0935 - 1020 

- the next hour will be used getting their raspberry Pi's functional and working. Make sure you instruct the class to use the offline image option when they setup their raspberry Pis. We do not want them to install their image over the internet because it will take too long.

### 1030 - 1130 (ensure there are 10 minute breaks every 20 - 25 minutes)

- Go over domain 1.1 Social engineering and start them on their lab after you complete the lesson.

### 1130 - 1300

- Lunch will be catered. We give our students two hour or an hour and half lunch. Just all depends on the instructor.
- Our catering person will be in contact with you, sometimes depending on where we pick up food lunch may come later than 1100.

### 1230 - 1330

- Go over Lesson 1.2 and 1.3 and start them on their lab for lesson 1.2 after the lesson is complete.

### 1330 - 1600

- Finish up lessons 1.4 - 1.6. There are currently no labs built for these lessons
