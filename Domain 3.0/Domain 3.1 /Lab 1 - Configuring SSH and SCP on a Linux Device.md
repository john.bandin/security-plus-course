# Lab 1 - Configuring SSH and SCP on a Linux Device
![](/Images/Linux%20Image%20(4).png)

## Lab Goals/Summary
- In this lab we will generate SSH keys and remote to our linux VM from our host computer
- We will download a package or software using the cURL command
- We will also cover usage for the SCP command.
- In this lab I am using a Debian-based OS (Kali Linux)

1. Login to the terminal on your Linux Device.

![](/Images/kali%20terminal.png)

2. Next we will generate our ssh keys by running the `ssh-keygen` command and following the instructions on screen. 

![](/Images/ssh%20key%20gen.png)

3. Now that we have generated an SSH private and public key-pairs, open up SecureCRT or Putty on your device. And run the `ip a` command on your linux machine to grab your Linux machines IP address. 

![](/Images/ip%20a%20command.png)

4. Now input that IP address into secureCRT or putty and SSH to your VM from the host mahcine.

![](/Images/securecrt%20snip.png)

5. Once you SSH to your machine exit out or continue in the SSH session for our next commands/steps. 

6. Now lets use the cURL command to download a file from a website. The command is `curl -o [website URL]`. The cURL command uses the HTTP application to transfer files.

![](/Images/curl%20command.png)

7. Now we are going to cover how to use the SCP command. `scp [options] source destination' This is the basic syntax for SCP. SCP is used to transfer files from host-->remote or remote--->host. 

![](/Images/scp%20command.png)

# Lab Summary

- This lab we covered how to generate SSH keys so our Linux machine can be remotely accessed using a secure protocol. We also covered how transfer files onto or from a remote system via CLI using SCP. 


